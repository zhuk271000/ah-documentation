.. default-domain:: sphinxsharp

QuizManager
===========

.. namespace:: AHG.QuizRedux

.. type:: public class QuizManager : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

Описание
--------

Этот класс является **менеджером викторины**.

.. attention:: Этот объект является `одиночкой <https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)>`_.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private GameObject questionPanel

    Ссылка на панель проведения викторины.

.. variable:: private GameObject questionImgObj;

    Ссылка на объект, для вопроса с изображением.

.. variable:: private Text questionText

    Ссылка на **UI Text**, который отображает текст вопроса.

.. variable:: private Text timerText

    Ссылка на **UI Text**, который отображает время таймера.

.. variable:: private CanvasGroup answerBtnsGroup

    Ссылка на канвас группу кнопок с вариантами ответов.

.. variable:: private Button[] answerBtns

    Ссылки на **UI Button**, которые отвечают за выбор варианта ответа.

.. variable:: private Image questionImage

    Ссылка на **UI Image**, который отображает изображение к вопросу.

.. variable:: private GameObject questionImageFullPanel

    Ссылка на панель показа изображения в полном размере.

.. variable:: private Image questionImageFull

    Ссылка на **UI Image**, который отображает изображение к вопросу в большем размере.

.. variable:: private Text questionImgText

    Ссылка на **UI Text**, который отображает текст вопроса с изображением.

.. variable:: private Image answerPanelImg

    Ссылка на **UI Image** панели показа состояния ответа.

.. variable:: private Text answerStateText

    Ссылка на **UI Text**, который отображает :enum:`тип ответа <AnswerType>` на вопрос.

.. variable:: private Image answerStateImage

    Ссылка на **UI Image**, который отображает иконку правильности ответа.

.. variable:: private Text questionCounterText

    Ссылка на **UI Text**, который отображает счётчик **текущий вопрос/всего вопросов**.

.. variable:: private Sprite[] answerStateIcons

    Ссылки на спрайты иконок правильности ответов. Порядок иконок должен соответствовать порядку :enum:`AnswerType`.

---------------

Свойства и поля
---------------

.. property:: public static QuizManager Instance { get; private set; }

    Свойство для доступа к **одиночному экземпляру** менеджера из любого класса.

----------------

Публичные методы
----------------

.. method:: public void Play(Category category)
    :param(1): Категория для старта игры.

    Метод для старта игры с указанной категорией.

.. method:: public void ToggleImageView(bool full)
    :param(1): Показать изображение в большем размере?

    Переключить показ изображения в большем размере.
    
    * ``true`` - Показать изображение в большем размере.
    * ``false`` - Вернуть изображение в прежнее состояние.

.. method:: public void SelectAnswer(int id)
    :param(1): Идентификатор ответа.

    Метод для выбора ответа.

----------------

Приватные методы
----------------

.. attention:: В документации перечисляется только список **важных приватных методов**. Описание остальных **написано комментариями** в коде проекта.

.. method:: private void GenerateQuestions()

    Метод для генерации нового списка вопросов.

.. method:: private void ShowQuestion()

    Показать случайный вопрос из списка.

.. method:: private void TimeOver()

    Метод, который вызывается при **окончании времени**.

    .. hint:: По стандарту пустоват, так что можете добавлять свои дополнительные действия.

    .. code-block:: csharp

        private void TimeOver()
        {
            // Тут можно добавить действия при окончании времени.
            ResetQuiz();
        }

.. method:: private void WrongAnswer()

    Метод, который вызывается при **неправильном ответе**.

    .. hint:: По стандарту пустоват, так что можете добавлять свои дополнительные действия.

    .. code-block:: csharp

        private void WrongAnswer()
        {
            // Тут можно добавить действия при неправильном ответе.
            ResetQuiz();
        }

.. method:: private void CorrectAnswer()

    Метод, который вызывается при **правильном ответе**.

    .. hint:: По стандарту пустоват, так что можете добавлять свои дополнительные действия.

    .. code-block:: csharp

        private void CorrectAnswer()
        {
            // Тут можно добавить действия при правильном ответе.
            GameManager.Instance.PlayAnimation(ANIM_HIDE_ANSWER, 2);
            ShowQuestion();
        }

.. method:: private void ResetQuiz()

    Восстановить викторину к начальному состоянию.

.. method:: private void ToggleTimer(bool start)
    :param(1): Запустить таймер?

    Переключить таймер.

    * ``true`` - Запустить таймер.
    * ``false`` - Остановить таймер.