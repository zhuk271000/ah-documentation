.. default-domain:: sphinxsharp

Config
======

.. namespace:: AHG.QuizRedux

.. type:: public class Config : ScriptableObject

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using UnityEngine;

Описание
--------

Класс конфигурации игры, в котором хранятся все категории и вопросы.

Свойства и поля
---------------

.. variable:: public List<Category> Categories;

    Список категорий.