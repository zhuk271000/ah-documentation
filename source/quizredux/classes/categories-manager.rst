.. default-domain:: sphinxsharp

CategoriesManager
=================

.. namespace:: AHG.QuizRedux

.. type:: public class CategoriesManager : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.Events;

Описание
--------

Этот класс является менеджером категорий.

.. attention:: Этот объект является `одиночкой <https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)>`_.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private GameObject categoriesPanel

    Ссылка на панель категорий.

.. variable:: private CanvasGroup categoriesCanvasGroup

    Ссылка на канвас группу категорий.

.. variable:: private GameObject categoryBtnPrefab

    Ссылка на префаб кнопки, которая создаётся в списке категорий.

.. variable:: private Transform categoryBtnsContent

    Ссылка на **Transform** компонент объекта, который является родителем для :var:`префабов кнопок <categoryBtnPrefab>`.

.. variable:: private Button nextPageBtn

    Ссылка на **UI Button**, которая переключает текущую страницу на следующую.

.. variable:: private Button prevPageBtn

    Ссылка на **UI Button**, которая переключает текущую страницу на предыдущую.

---------------

Свойства и поля
---------------

.. property:: public static CategoriesManager Instance { get; private set; }

    Свойство для доступа к **одиночному экземпляру** менеджера из любого класса.

-------------------------

Константы
---------

.. variable:: private const int PAGE_BTTNS_COUNT = 4

    Количество кнопок на страницу.

----------------

Публичные методы
----------------

.. method:: public void Open()

    Открыть панель выбора категорий.

.. method:: public void Close()

    Закрыть панель выбора категорий.

.. method:: public void ChangePage(int dir)
    :param(1): Направление смены страницы.

    Переключить страницу списка категорий, в зависимости от направления.

    * ``-1`` - прошлая страница.
    * ``1`` - следующая страница.

----------------

Приватные методы
----------------

.. attention:: В документации перечисляется только список **важных приватных методов**. Описание остальных **написано комментариями** в коде проекта.

.. method:: private void LoadPage()

    Загрузить страницу категорий.

.. method:: private void ClearList()

    Очистить пул кнопок, и сбросить текущую страницу на 0.