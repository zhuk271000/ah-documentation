.. default-domain:: sphinxsharp

Category
========

.. namespace:: AHG.QuizRedux

.. type:: public class Category

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using System;

Описание
--------

Этот класс хранит информацию о категории.

Свойства и поля
---------------

.. variable:: public string Name

    Название категории.

.. variable:: public List<Question> Questions

    Список вопросов.

.. variable:: public int AnswerTime = 15

    Время для ответа на вопрос.