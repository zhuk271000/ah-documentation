.. default-domain:: sphinxsharp

Question
========

.. namespace:: AHG.QuizRedux

.. type:: public class Question

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using UnityEngine;
    using System;

Описание
--------

Этот класс хранит информацию о вопросе.

Свойства и поля
---------------

.. variable:: public string Text

    Текст вопроса.

.. variable:: public Sprite Image

    Картинка вопроса (не обязательна).

.. variable:: public string[] Answers

    Список вариантов ответов.

.. variable:: public int CorrectAnswerID

    Идентификатор правильного ответа.

------------

Конструкторы
------------

.. method:: public Question(string text, Sprite image, string[] answers, int correctAnswerID)
    :param(1): Текст вопроса.
    :param(2): Картинка вопроса.
    :param(3): Список вариантов ответов.
    :param(4): Идентификатор правильного ответа.