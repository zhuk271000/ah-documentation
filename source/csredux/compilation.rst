Компиляция игры
===============

После того как вы сделали все нужные изменения в проекте, можно его скомпилировать для проверки на целевой платформе.

.. note:: Сейчас будет рассмотрена компиляция на платформу **Android**.

Переходим в *File/Build Settings*, и добавляем нашу игровую сцену в **Scenes In Build**.

.. hint:: Можете нажать на кнопку **Add Open Scenes**, для добавления открытой сцены.

.. figure:: ../_static/csredux/anim1.gif
    :alt: Add Open Scenes Gif
    :align: center

Далее переходим в окно **Player Settings** для настройки самой компиляции.

.. warning:: Для компиляции под Android вам надо установить **Android SDK** через SDK Manager, который можете найти в Android Studio.
    Более подробно в `документации Android разработчиков <https://developer.android.com/studio/intro/update#sdk-manager>`_.

И настраиваем все главные поля:

.. note:: Далее будет информация только о главных полях для компиляции. Информацию о других, можете прочитать в `документации Unity <https://docs.unity3d.com/Manual/class-PlayerSettingsAndroid.html>`_.

Company Name
    Название вашей компании. *Например: Android Helper Games*

Product Name
    Название игры. *Например: Case Simulator Redux*

В разделе **Resolution and Presentation** ставим галочки только на ландшафтные ориентации.

.. figure:: ../_static/csredux/image18.png
    :align: center

В разделе **Other Settings** пишете свой **Package Name**, и изменяете версию игры если надо. Так же можете настроить минимальный и максимальный уровень **API**.

.. figure:: ../_static/csredux/image17.png
    :align: center

    Тут показаны рекомендуемые настройки конфигурации.

Остальные настройки можете настраивать по своему усмотрению.

После того как всё нужное настроили, можем переходить назад к окну **Build Settings**, и нажимать на кнопку **Build**.
Либо **Build And Run**, если у вас подключено устройство по USB, чтобы запустить игру на нём.

.. figure:: ../_static/csredux/image19.png
    :align: center

Если всё сделали правильно, и все нужные компоненты установлены для компиляции, то она пройдёт без ошибок.