.. default-domain:: sphinxsharp

CaseOpener
==========

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class CaseOpener : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

Описание
--------

Этот класс управляет открытием кейсов.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

Настройки рулетки
"""""""""""""""""

.. variable:: private int generateWeaponsCount = 50

    Количество оружий, которое будет создано в рулетке.

.. variable:: private float defaultRouletteSpeed

    Стартовая скорость рулетки.

.. variable:: private Vector2 rouletteSlowdownRange = new Vector2(4, 6)

    Диапозон скорости торможения рулетки.

    * ``x`` - минимальное значение торможения.
    * ``y`` - максимальное значение торможения.

Объекты
"""""""

.. variable:: private GameObject caseOpenerPanel

    Ссылка на панель открытия кейса.

.. variable:: private GameObject weaponPreviewPrefab

    Ссылка на превью префаб оружия, которое показывается в списке, при выборе кейса.

.. variable:: private GameObject rouletteWeaponPrefab

    Ссылка на префаб оружия, которое будет создаваться в рулетке.

.. variable:: private Transform rouletteWeaponsContent

    Ссылка на **Transform** компонент объекта, который является родителем для :var:`префабов оружий рулетки <rouletteWeaponPrefab>`.

.. variable:: private Transform weaponsPreviewContent

    Ссылка на **Transform** компонент объекта, который является родителем для :var:`префабов оружий <weaponPreviewPrefab>`, при выборе кейса.

.. variable:: private Transform centerLineTransform

    Ссылка на **Transform** компонент объекта, который является центральной линией в рулетке.

.. variable:: private Button openCaseBtn

    Ссылка на **UI Button**, который служит кнопкой для открытия кейса.

.. variable:: private Text openCaseBtnText

    Ссылка на **UI Text**, для текста на кнопке :var:`открытия кейса <openCaseBtn>`.

.. variable:: private Button backBtn

    Ссылка на **UI Button**, которая служит кнопкой возвращения к прошлой панели.

.. variable:: private GameObject dropPanel

    Ссылка на панель выпадения оружия.

.. variable:: private Image dropImg

    Ссылка на **UI Image**, для отображения оружия, которое выпало из кейса.

.. variable:: private Text dropTitle

    Ссылка на **UI Text**, для текста в заголовке панели :var:`выпадения оружия <dropPanel>`.

.. variable:: private Text dropCostText

    Ссылка на **UI Text**, для отображения цены оружия, которое выпало из кейса.

---------------

Публичные методы
----------------

.. method:: public void ShowOpenerPanel(int caseId)
    :param(1): Порядковый идентификатор кейса.

    Показать панель открытия кейса, идентификатор которого указан в параметрах.

    .. warning:: При указании несуществующего идентификатора, вы получите **ошибку**.

.. method:: public void CloseOpenerPanel()

    Закрыть панель открытия кейса.

.. method:: public void OpenCase()

    Открыть выбранный кейс.

    .. attention:: Если кейсы покупаются за игровую валюту, то при её нехватке **появится уведомление**.

.. method:: public void SellDrop()

    Продать оружие, которое выпало из кейса.

.. method:: public void TakeDrop()

    Забрать оружие, которое выпало из кейса.

----------------

Приватные методы
----------------

.. attention:: В документации перечисляется только список **важных приватных методов**. Описание остальных **написано комментариями** в коде проекта.

.. method:: private void Clear()

    Очистить рулетку и превью оружий.

.. method:: private void GenerateCasePreviews()

    Сгенерировать превью оружий.

.. method:: private void FillRoulette()

    Заполнить рулетку оружиями.

.. method:: private int GetRandomRarity(List<float> chances)
    :param(1): Шансы выпадения редкостей

    Возвращает случайную редкость оружия.

.. method:: private void ShowDrop(Weapon weapon)
    :param(1): Экземпляр оружия, которое выпало.

    Показать оружие, которое выпало из кейса.