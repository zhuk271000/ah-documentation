.. default-domain:: sphinxsharp

AlertsManager
=============

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class AlertsManager : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.Events;

Описание
--------

Этот класс служит для **управления уведомлениями**.

.. attention:: Этот объект является `одиночкой <https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)>`_.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private GameObject alertsWindow

    Ссылка на панель уведомления.

.. variable:: private GameObject defaultPanel

    Ссылка на **стандартную** панель уведомления.

.. variable:: private GameObject doubleOptionsPanel

    Ссылка на панель уведомления **с двумя вариантами** действий.

.. variable::  private Button[] doubleOptionsBtns

    Ссылка на массив **UI Button**, которые служат кнопками действий для :var:`нестандартной панели уведомления <doubleOptionsPanel>`.

.. variable:: private Text alertText

    Ссылка на **UI Text**, для отображения текста уведомления.

---------------

Свойства и поля
---------------

.. property:: public static AlertsManager Instance { get; private set; }

    Свойство для доступа к **одиночному экземпляру** менеджера из любого класса.

------

Публичные методы
----------------

.. method:: public void Show(AlertType type, string text, UnityAction onClickYes, UnityAction onClickNo)
    :param(1): Тип панели уведомления.
    :param(2): Текст уведомления.
    :param(3): Действие при нажатии на кнопку "да" (если уведомление с двумя вариантами действий).
    :param(4): Действие при нажатии на кнопку "нет" (если уведомление с двумя вариантами действий).

    Показать уведомление.

.. method:: public void Show(AlertType type = AlertType.Default, string text = null)
    :param(1): Тип панели уведомления.
    :param(2): Текст уведомления.

    Показать уведомление.

.. method:: public void Close()

    Закрыть панель уведомления.