.. default-domain:: sphinxsharp

Inventory
=========

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class Inventory : PageSelectorBase

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.Events;

Описание
--------

:type:`Селектор <PageSelectorBase>` с переключением страниц **для инвентаря игрока**.

.. attention:: Полный список переменных, методов и т.д., находится в **классе-родителе**.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private GameObject inventoryPanel

    Ссылка на панель инвентаря.

.. variable:: private GameObject pageDotPrefab

    Ссылка на префаб *"точки"* для пересчёта страниц.

.. variable:: private Transform pageDotsContent

    Ссылка на **Transform** компонент объекта, который является родителем для :var:`точек пересчёта страниц <pageDotPrefab>`.

    .. figure:: /_static/csredux/image15.png
        :align: center

        Точки пересчёта страниц **находятся справа**.

---------------

Свойства и поля
---------------

.. property:: protected override int SlotsCount {get;}

    Общее количество слотов равно количеству оружий в инвентаре игрока: ``Data.Player.Weapons.Count``

------

Публичные методы
----------------

.. method:: public void Show()

    Показать панель инвентаря и загрузить страницы.

.. method:: public void Close()

    Закрыть панель инвентаря.

.. method:: public void Refresh()

    Обновить инвентарь.

-----------------------

Приватные методы
----------------

.. attention:: В документации перечисляется только список **важных приватных методов**. Описание остальных **написано комментариями** в коде проекта.

.. method:: private void OnWeaponClick(int id, Weapon weapon)
    :param(1): Порядковый идентификатор оружия в инвентаре.
    :param(2): Экземпляр оружия.

    Метод при нажатии на оружие.

-----------------------

Переопределённые методы
-----------------------

.. method:: public override void Load()

    Инициализировать слоты и открыть первую страницу.

.. method:: public override void Clear()

    Очистить пул слотов.

.. method:: protected override void InitializeSlot(int index, int id, GameObject slot)
    :param(1): Порядковый индекс слота.
    :param(2): Идентификатор слота относительно общего кол-ва слотов.
    :param(3): Объект слота.

    Этот метод обрабатывает слот как :type:`WeaponViewer`.

.. method:: public override void ChangePage(int dir)
    :param(1): Направление смены страницы.

    Сменить страницу в зависимости от направления.

    * ``-1`` - прошлая страница.
    * ``1`` - следующая страница.