.. default-domain:: sphinxsharp

WeaponViewer
============

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class WeaponViewer : MonoBehaviour

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.Events;

Описание
--------

Этот класс должен находиться на **префабе** оружия, для его отображения и дальнейшей обработки кликов.

Приватные сериализуемые поля
----------------------------

.. note:: Все сериализуемые значения изменяются в **инспекторе**.

.. variable:: private Image icon

    Ссылка на **UI Image**, для изображения оружия.

---------------

Свойства и поля
---------------

.. property:: public int ID { get; set; }

    Идентификатор оружия.

.. property:: public Image Icon { get; }

    Ссылка на **UI Image**, для иконки кейса.

------

Публичные методы
----------------

.. method:: public Weapon GetWeapon()

    Возвращает экземпляр оружия.

.. method:: public void SetWeapon(Weapon weapon, UnityAction<int, Weapon> onClick)
    :param(1): Экземпляр оружия.
    :param(2): Действие при клике на оружие.

    Инициализировать **WeaponViewer** указанным экземпляром оружия.

.. method:: public void SetWeapon(Weapon weapon)
    :param(1): Экземпляр оружия.

    Инициализировать **WeaponViewer** указанным экземпляром оружия.

.. method:: public void OnClick()

    Метод при нажатии на оружие.