.. default-domain:: sphinxsharp

CaseSelector
============

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public class CaseSelector : PageSelectorBase

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using UnityEngine;

Описание
--------

:type:`Селектор <PageSelectorBase>` с переключением страниц **для выбора кейсов**.

.. attention:: Полный список переменных, методов и т.д., находится в **классе-родителе**.

Свойства и поля
---------------

.. property:: protected override int SlotsCount {get;}

    Общее количество слотов равно количеству кейсов: ``GameManager.GetConfig().GetCasesCount()``

-----------------------

Переопределённые методы
-----------------------

.. method:: protected override void InitializeSlot(int index, int id, GameObject slot)
    :param(1): Порядковый индекс слота.
    :param(2): Идентификатор слота относительно общего кол-ва слотов.
    :param(3): Объект слота.

    Этот метод обрабатывает слот как :type:`CaseViewer`.