.. default-domain:: sphinxsharp

Save
====

.. namespace:: AHG.CaseSimulatorRedux

.. type:: public struct Save

Подключенные библиотеки:
------------------------

.. code-block:: csharp

    using System.Collections.Generic;

Описание
--------

Структура данных игрока, которая разбирается в **JSON** строку, и в дальнейшем может быть сохранена.

Свойства и поля
---------------

.. variable:: public double Money

    Игровая валюта игрока.

.. variable:: public List<string> Weapons

    Список оружий из инвентаря, сохраняемые в виде ``CaseID-WeaponID``.

    * ``CaseID``- Порядковый идентификатор кейса, в котором находится оружие.
    * ``WeaponID`` - Порядковый идентификатор оружия.

------------

Конструкторы
------------

.. method:: public Save(double money, Dictionary<int, Weapon> weapons)
    :param(1): Текущее значение кол-ва валюты у игрока.
    :param(2): Словарь оружий из инвентаря.